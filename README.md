RFC 8, 9 and 51 are not provided in a plain text format.

```bash
for i in {1..7803}
do
   [[ -r "rfc$i.txt" ]] || echo "$i"
done
```
